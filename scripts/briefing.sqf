// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

// TO USE THIS FILE. UNCOMMENT THE MARKED LINE IN "initPlayerLocal.sqf"

player createDiaryRecord ["Diary", ["Notes",
"As the situation indicates, the tanks do not have thermal optics. Working together with recon teams will be the best way to locate and destroy targets.
<br/><br/>The tanks have a custom damage handler for this mission. They cannot die, but will be rendered inoperable should they take a lethal amount of damage. At that point, the only way to restore the tank to an operational state will be a full repair from logistics.
<br/><br/>For Tank Commanders (TCs): You have an option to 'Switch to GPSE' when in the tank. This will show you exactly what the gunner is looking at.
<br/><br/>For Gunners and TCs: The new CUP M1s have a new ammo handling system. You start with 42 rounds total, which are a mix of HEAT and APFSDS rounds. To see how many rounds of each type you have left, you will need to use displays that are visible from within the tank.
<br/><br/>For Gunners and TCs: When the tank is rearmed, you can use the 'Reset ammo store display' option if your ammo displays do not update properly.
<br/><br/>There is a flagpole at the base that can be used to teleport to the command vehicle if you die. If the TP doesn't work, it means that the command vehicle is full, or has been destroyed."
]];

player createDiaryRecord ["Diary", ["Assets", "For this mission, you will have access to:
	<br/>- 1x M1A2SEPv1 (TUSK II) (Command Tank)
	<br/>- 3x M1A2SEPv1
	<br/>- 2x Humvee M2
	<br/>- 2x Humvee 4-seater cargo
	<br/>- 1x Humvee Repair/Refuel
	<br/>- 1x Humvee Rearm
	<br/>- 1x Humvee 2-seater cargo
	<br/>- 1x Humvee Shelter Carrier (command vehicle)
"]];

player createDiaryRecord ["Diary", ["Intel",
"We know that the Chedaki have backing from the Russian military, since there's no other way that they could have gotten all of this equipment. Be on the lookout for advanced Russian armour as you near Severograd."
]];


player createDiaryRecord ["Diary", ["Mission",
"Your mission today is to destroy the advanced 'AA Control System' located somewhere near Severograd. This will allow friendly aircraft to enter the area to provide fire support."
]];

player createDiaryRecord ["Diary", ["Situation",
"Russian-backed Chedaki forces have taken control of north-western Chernarus, and have deployed a highly advanced anti-air control system in the area. This system means that we couldn't get any air support in the area to stop their advance.
<br/><br/>Existing friendly forces in the area have all been wiped out, with the only friendly forces remaining in the area being a tank platoon, a recon squad, and a logistics team that were on a training exercise in the woods to the west.
<br/><br/>The equipment you have is the best that we have left to use. Unfortunately, the thermal sights on the tanks are all inoperative, but the recon teams are equipped with high-zoom thermal optics. You should still be able to spot targets if you work together."
]];
