// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	class example
	{
		displayName = "Example Loadout"; // Currently unused, basically just a human-readable name for the loadout
		
		// Weapon definitions all use the following format:
		// {Weapon Classname, Suppressor, Pointer (Laser/Flashlight), Optic, [Primary magazine, ammo count], [Secondary Magazine (GL), ammo count], Bipod}
		// Any empty definitions must be defined as an empty string, or an empty array. Otherwise the loadout will not apply correctly.
		
		primaryWeapon[] = {"arifle_MXC_F", "", "acc_flashlight", "optic_ACO", {"30Rnd_65x39_caseless_mag",30}, {}, ""}; // Primary weapon definition
		secondaryWeapon[] = {"launch_B_Titan_short_F", "", "", "", {"Titan_AP", 1}, {}, ""}; // Secondary weapon (Launcher) definition.
		handgunWeapon[] = {"hgun_ACPC2_F", "", "", "", {"9Rnd_45ACP_Mag", 9}, {}, ""}; // Handgun definition
		binocular = "Binocular";
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_Watchcap_blk";
		facewearClass = "";
		vestClass = "V_Chestrig_khk";
		backpackClass = "B_AssaultPack_mcamo";
		
		// Linked items requires all six definitions to be present. Use empty strings if you do not want to add that item.
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", ""}; // Linked items for the unit, must follow the order of: Map, GPS, Radio, Compass, Watch, NVGs.
		
		// When placed in an item array, magazines should also have their ammo count defined
		uniformItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	class example_random
	{
		displayName = "Random Loadouts";
		class random_1
		{
			// Loadout info goes here
		};
		class random_2
		{
			// Loadout info goes here
		};
	};
	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"hgun_Pistol_heavy_01_green_F", "", "acc_flashlight_pistol", "", {"11Rnd_45ACP_Mag", 11}, {}, ""};
		binocular = "Binocular";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemMicroDAGR", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"11Rnd_45ACP_Mag",1,11},{"ItemcTabHCam",1}};
		vestItems[] = {{"SmokeShell",4,1},{"SmokeShellBlue",4,1},{"CUP_30Rnd_556x45_PMAG_QP_Tracer_Red",7,30}};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",1},{"ACE_tourniquet",2},{"ACE_bloodIV_500",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Crew_F: base {
		displayName = "Crewman";
		
		primaryWeapon[] = {"CUP_arifle_SBR_black", "", "cup_acc_flashlight", "cup_optic_ac11704_black", {"CUP_30Rnd_556x45_PMAG_QP_Tracer_Red",30}, {}, ""};
		
		uniformClass = "CUP_U_CRYE_G3C_M81_RGR";
		headgearClass = "CUP_H_CVC";
		facewearClass = "CUP_G_ESS_BLK";
		vestClass = "CUP_V_B_Interceptor_Base_Olive";
		backpackClass = "B_AssaultPack_rgr";
		
		backpackItems[] = {{"ToolKit",1}};
	};
	
	class crew_lead: B_Crew_F {
		displayName = "Crew Lead";
		
		linkedItems[] = {"ItemMap", "ItemAndroid", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};

		facewearClass = "G_Aviator";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
	};
	
	class recon_base: base {
		displayName = "Recon Base";
		binocular = "CUP_SOFLAM";
		
		primaryWeapon[] = {"CUP_arifle_SBR_black", "", "cup_acc_flashlight", "optic_mrco", {"CUP_30Rnd_556x45_PMAG_QP_Tracer_Red",30}, {}, ""};
		
		uniformClass = "CUP_U_CRYE_G3C_RGR";
		headgearClass = "CUP_H_OpsCore_Spray_US_SF";
		facewearClass[] = {"CUP_G_ESS_BLK_Scarf_Grn","CUP_G_ESS_BLK_Scarf_Red","CUP_G_ESS_KHK_Scarf_Tan","CUP_G_ESS_BLK_Scarf_White"};
		vestClass = "CUP_V_CPC_Fastbelt_coy";
	};
	
	class B_Soldier_SL_F: recon_base {
		displayName = "Recon Base";
		
		linkedItems[] = {"ItemMap", "ItemAndroid", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};

		facewearClass[] = {"CUP_G_ESS_KHK_Scarf_Tan_Beard","CUP_G_ESS_KHK_Scarf_Tan_Beard_Blonde"};
		vestClass = "CUP_V_CPC_tlbelt_coy";
		backpackClass = "FRXA_tf_rt1523g_Ranger_Green";
		
		vestItems[] = {{"SmokeShell",4,1},{"SmokeShellBlue",4,1},{"CUP_30Rnd_556x45_PMAG_QP_Tracer_Red",7,30},{"ACE_Kestrel4500",1},{"ACE_RangeCard",1}};
		backpackItems[] = {{"ACE_SpottingScope",1},{{"ACE_VectorDay", "", "", "", {}, {}, ""},1}};
	};
	
	class B_Soldier_AAT_F: recon_base {
		displayName = "Recon Assistant Missile Specialist";

		backpackClass = "CUP_B_USPack_Coyote";
		
		backpackItems[] = {{"Titan_AT",2,1},{{"ACE_VectorDay", "", "", "", {}, {}, ""},1}};
	};
	
	class B_Soldier_AT_F: recon_base {
		displayName = "Recon Missile Specialist";
		secondaryWeapon[] = {"launch_O_Titan_short_F", "", "acc_flashlight", "", {"Titan_AT", 1}, {}, ""};

		backpackClass = "CUP_B_AssaultPack_Coyote";
		
		backpackItems[] = {{"Titan_AT",1,1},{{"ACE_VectorDay", "", "", "", {}, {}, ""},1}};
	};
	
	class B_Sharpshooter_F: recon_base {
		displayName = "Recon Sniper";
		
		primaryWeapon[] = {"srifle_LRR_F", "", "cup_acc_flashlight", "optic_lrps", {"7Rnd_408_Mag",7}, {}, ""};
		
		vestItems[] = {{"SmokeShell",4,1},{"ACE_Kestrel4500",1},{"ACE_RangeCard",1},{"SmokeShellBlue",4,1},{"7Rnd_408_Mag",4,7}};
		backpackItems[] = {{"ACE_Tripod",1},{"7Rnd_408_Mag",5,7},{{"ACE_VectorDay", "", "", "", {}, {}, ""},1}};
		backpackClass = "B_AssaultPack_cbr";
	};
	
	class B_Officer_F: recon_base {
		displayName = "Recon Command/Driver";
		
		linkedItems[] = {"ItemMap", "ItemcTab", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};


		facewearClass[] = {"CUP_G_ESS_KHK_Scarf_Tan_Beard","CUP_G_ESS_KHK_Scarf_Tan_Beard_Blonde"};
		headgearClass = "CUP_H_PMC_Cap_Back_EP_Tan";
		vestClass = "CUP_V_CPC_communicationsbelt_coy";
		backpackClass = "FRXA_tf_rt1523g_Ranger_Green";
	};
	
	class B_Engineer_F: base {
		displayName = "Logistics Engineer";
		
		primaryWeapon[] = {"CUP_arifle_SBR_black", "", "cup_acc_flashlight", "optic_mrco", {"CUP_30Rnd_556x45_PMAG_QP_Tracer_Red",30}, {}, ""};
		
		uniformClass = "CUP_U_CRYE_G3C_RGR";
		headgearClass = "CUP_H_PMC_Cap_PRR_Tan";
		facewearClass = "G_Lowprofile";
		vestClass = "CUP_V_CPC_Fastbelt_coy";
		backpackClass = "CUP_B_USMC_AssaultPack";
		
		backpackItems[] = {{"ToolKit",1}};
		
		basicMedBackpack[] = {{"ACE_bloodIV_500",5},{"ACE_epinephrine",5},{"ACE_morphine",5},{"ACE_fieldDressing",40}};
	};
	
	class logi_lead: B_Engineer_F {
		displayName = "Logi Lead";
		
		linkedItems[] = {"ItemMap", "ItemAndroid", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};

		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
	};
};