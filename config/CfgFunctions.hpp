// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

// If you need to add additional functions, create a new section below using your tag of choice (Ex: SXP = Superxpdude)
// See the functions library wiki page for additional details

class SXP {
	class mission {
		class humveeWheelEH {postInit = 1;}; // Humvee wheel animation
		class setupFlagPole {}; // Flagpole setup function
		class setupVehicle {}; // Vehicle setup function
		class tankHandleDamage {}; // Tank damage handling
	};
};