// SXP_fnc_tankHandleDamage
// Advanced damage handling for the M1 Abrams
params ["_unit", "_selection", "_damage", "_source", "_projectile", "_hitIndex", "_instigator", "_hitPoint"];

//systemChat format ["DMG: %1|%2", _selection, _damage];

if ((_selection isEqualTo "") && {_damage > 0.95}) then {
	// Kill the engine and tracks to mobility kill the tank
	_unit setHitPointDamage ["hitEngine", 1];
	_unit setHitPointDamage ["hitLTrack", 1];
	_unit setHitPointDamage ["hitRTrack", 1];
	// Override damage handling so that the tank can't actually die
	0.95
};