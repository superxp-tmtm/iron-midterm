// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Make sure that respawned vehicles are editable by zeus
{
	_x addCuratorEditableObjects [[_newVeh],true];
} forEach allCurators;

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	case (toLower "CUP_B_M1A2SEP_Woodland_US_Army"): {
		// Handle barrel art on respawn
		if (!isNil "_oldVeh") then {
			{
				_newVeh setVariable [_x#0, _oldVeh getVariable [_x#0,_x#1], true];
			} forEach [
				["CUP_M1Abrams_BA_Decal",-1],
				["CUP_M1Abrams_BA_File_1",""],
				["CUP_M1Abrams_BA_File_2",""],
				["CUP_M1Abrams_TankNumber",-1],
				["CUP_M1Abrams_ArmyChevron",-1]
			];
			
			if (_newVeh getVariable ["CUP_M1Abrams_BA_Decal",-1] == -2) then {
				private _barrelArtFrontSelection = [_newVeh, "barrel_art_front"] call CUP_fnc_hiddenSelectionByName;
				private _barrelArtRearSelection = [_newVeh, "barrel_art_rear"] call CUP_fnc_hiddenSelectionByName;
				_newVeh setObjectTextureGlobal [_barrelArtFrontSelection, _newVeh getVariable ["CUP_M1Abrams_BA_File_1",""]];
				_newVeh setObjectTextureGlobal [_barrelArtRearSelection, _newVeh getVariable ["CUP_M1Abrams_BA_File_2",""]];
			};
		};
		
		// Disable thermals
		_newVeh disableTIEquipment true;
		
		// Damage handling
		[_newVeh, ["HandleDamage", SXP_fnc_tankHandleDamage]] remoteExec ["addEventHandler", 0, _newVeh];;
		
		// Add a second spare track
		[_newVeh, 1, "ACE_Track", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	// Command Tank
	case (toLower "CUP_B_M1A2SEP_TUSK_II_Woodland_US_Army"): {
		// Handle barrel art on respawn
		if (!isNil "_oldVeh") then {
			{
				_newVeh setVariable [_x#0, _oldVeh getVariable [_x#0,_x#1], true];
			} forEach [
				["CUP_M1Abrams_BA_Decal",-1],
				["CUP_M1Abrams_BA_File_1",""],
				["CUP_M1Abrams_BA_File_2",""],
				["CUP_M1Abrams_TankNumber",-1],
				["CUP_M1Abrams_ArmyChevron",-1]
			];
			
			if (_newVeh getVariable ["CUP_M1Abrams_BA_Decal",-1] == -2) then {
				private _barrelArtFrontSelection = [_newVeh, "barrel_art_front"] call CUP_fnc_hiddenSelectionByName;
				private _barrelArtRearSelection = [_newVeh, "barrel_art_rear"] call CUP_fnc_hiddenSelectionByName;
				_newVeh setObjectTextureGlobal [_barrelArtFrontSelection, _newVeh getVariable ["CUP_M1Abrams_BA_File_1",""]];
				_newVeh setObjectTextureGlobal [_barrelArtRearSelection, _newVeh getVariable ["CUP_M1Abrams_BA_File_2",""]];
			};
		};
		
		// Add antennas
		[
			_newVeh,
			["USWoodland",1], 
			["hide_bustle_rack_ext",0,"hide_duke_antennas",0,"hide_cip_panel_bustle",0]
		] call BIS_fnc_initVehicle;
		
		// Disable thermals
		_newVeh disableTIEquipment true;
		
		// Damage handling
		[_newVeh, ["HandleDamage", SXP_fnc_tankHandleDamage]] remoteExec ["addEventHandler", 0, _newVeh];;
		
		// Add a second spare track
		[_newVeh, 1, "ACE_Track", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	
	// Shelter Carrier Humvee
	case (toLower "CUP_B_nM1037sc_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_rear_left_antenna",1,"hide_rear_right_antenna",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",1,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",1,"hide_radio_large",0,"hide_old_front_bumper",1,"hide_old_rear_bumper",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0]
		] call BIS_fnc_initVehicle;
		
		// Large radio screen
		_newVeh setObjectTextureGlobal [17, "media\doItForHer.paa"];
		
		// Shelter Carrier Monitors
		_newVeh setObjectTextureGlobal [19, "media\doItForHer.paa"];
		_newVeh setObjectTextureGlobal [20, "media\doItForHer.paa"];
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	
	// 2-seater pickup Humvee
	case (toLower "CUP_B_nM1038_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_rear_left_antenna",0,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",0,"hide_jerrycans",1,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",1,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0,"hide_bench_rotate",0,"unhide_Deployment_1",0,"unhide_Deployment_2",0]
		] call BIS_fnc_initVehicle;
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	
	// Recon Humvee
	case (toLower "CUP_B_nM1038_4s_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_rear_left_antenna",1,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",0,"hide_door_front_left",0,"hide_door_front_right",0,"hide_door_rear_left",0,"hide_door_rear_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",1,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_front_roof",0,"unhide_Deployment_1",0,"unhide_Deployment_2",1,"hide_roll_up_tarp",0]
		] call BIS_fnc_initVehicle;
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "recon_truck"] call XPT_fnc_loadItemCargo;
	};
	
	// M2 Humvee
	case (toLower "CUP_B_nM1025_M2_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_front_left_antenna",1,"hide_front_right_antenna",1,"hide_rear_left_antenna",1,"hide_rear_right_antenna",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",1,"hide_jerrycans",1,"hide_spare_wheel",0,"hide_spare_wheel_mount",1,"hide_door_front_left",0,"hide_door_front_right",0,"hide_door_rear_left",0,"hide_door_rear_right",0,"hide_ammo_cans",1,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",1,"hide_old_rear_bumper",1]
		] call BIS_fnc_initVehicle;
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	
	// Repair Humvee
	case (toLower "CUP_B_nM1038_Repair_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_rear_left_antenna",1,"hide_rear_right_antenna",0,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",1,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",1,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0,"hide_bench_rotate",0,"unhide_Deployment_1",0,"unhide_Deployment_2",0]
		] call BIS_fnc_initVehicle;
		
		[_newVeh, 12] call ace_cargo_fnc_setSpace;
		[_newVeh, 4, "ACE_Track", true] call ace_repair_fnc_addSpareParts;
		[_newVeh, 3, "ACE_Wheel", true] call ace_repair_fnc_addSpareParts;
		
		[_newVeh, -10, [[-1.12,-2.5,-0.78],[-1.12,-2.34,-0.78]]] call ace_refuel_fnc_makeSource;
		
		[_newVeh, "tank"] call XPT_fnc_loadItemCargo;
	};
	
	// Ammo Humvee
	case (toLower "CUP_B_nM1038_Ammo_USA_WDL"): {
		[
			_newVeh,
			["USWoodland",1], 
			["hide_rear_left_antenna",1,"hide_rear_right_antenna",1,"hide_left_mirror",0,"hide_right_mirror",0,"hide_brushguard",1,"hide_blue_force_tracker",1,"hide_jerrycans",0,"hide_spare_wheel",0,"hide_spare_wheel_mount",1,"hide_door_front_left",0,"hide_door_front_right",0,"hide_cip",1,"hide_rear_view_camera",0,"hide_radio_small",0,"hide_radio_large",1,"hide_old_front_bumper",1,"hide_old_rear_bumper",1,"hide_rear_tarp",1,"hide_rear_tarp_holder",1,"hide_middle_tarp",0,"hide_middle_wall",0,"hide_front_roof",0,"hide_bench_rotate",0,"unhide_Deployment_1",0,"unhide_Deployment_2",0]
		] call BIS_fnc_initVehicle;
		
		[_newVeh, "recon_truck"] call XPT_fnc_loadItemCargo;
	};
	
	// Humvee large radio monitor
	//expression = "if (local _this) then {_this setObjectTextureGlobal [17, _value]}";
};