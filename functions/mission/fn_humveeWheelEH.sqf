// SXP_fnc_humveeWheelEH
// Hides or unhides the humvee spare tire depending on if the ACE cargo has one.



// CUP_nHMMWV_Base
private _humveeWheelFunc = {
	params ["_item", "_vehicle"];
	
	private _isWheel = {
		if (_this isEqualType "") then {
			_this == "ace_wheel";
		} else {
			_this isKindOf "ACE_Wheel";
		};
	};
	
	if ((!local _vehicle) or {!(_vehicle isKindOf "CUP_nHMMWV_Base")}) exitWith {};
	private _animationConfigs = (configProperties [(configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "AnimationSources")]) apply {configName _x};
	if ("hide_spare_wheel" in _animationConfigs) then {
		// Check to see if we have any wheels left
		if (({_x call _isWheel} count (_vehicle getVariable ["ace_cargo_loaded",[]])) > 0) then {
			_vehicle animateSource ["hide_spare_wheel",0,true];
			_vehicle animateSource ["hide_spare_wheel_mount",0,true];
		} else {
			_vehicle animateSource ["hide_spare_wheel",1,true];
		};
	};
	
};

["ace_cargoLoaded", _humveeWheelFunc] call CBA_fnc_addEventHandler;
["ace_cargoUnloaded", _humveeWheelFunc] call CBA_fnc_addEventHandler;